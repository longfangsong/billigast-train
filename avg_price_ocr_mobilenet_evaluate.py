import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import transforms, datasets
from torch.utils.data import Dataset, DataLoader
import pandas as pd
from PIL import Image
from torchvision.models import mobilenet_v3_large, MobileNet_V3_Large_Weights

device = torch.device("cpu")


class CustomDigitDataset(Dataset):
    def __init__(self, csv_file, transform=None):
        self.data = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        img_path = self.data.iloc[idx, 0]
        digits = self.data.iloc[idx, 2:8].values.astype('int')
        data = [torch.nn.functional.one_hot(
                torch.tensor(x), num_classes=10) for x in digits
                ]
        digits = torch.stack(
            data
        )
        digits = digits.float()
        # Load image
        image = Image.open(img_path)
        image = image.convert('RGB')
        image = image.resize((640, 640))
        if self.transform:
            image = self.transform(image)

        return image, digits


model = mobilenet_v3_large(weights=MobileNet_V3_Large_Weights.DEFAULT)


class Classifier(nn.Module):
    def __init__(self):
        super(Classifier, self).__init__()
        classifier = [
            nn.Sequential(
                nn.Linear(960, 1280),
                nn.Hardswish(),
                nn.Dropout(p=0.2, inplace=True),
                nn.Linear(1280, 10)  # Output for each digit (0-9)
            ) for _ in range(6)
        ]
        self.classifier = nn.ModuleList(classifier)

    def forward(self, x):
        batch_size = x.size(0)
        classifier_outputs = []
        for i in range(6):
            classifier_output = self.classifier[i](x)
            classifier_outputs.append(classifier_output)
        classifier_outputs = torch.stack(classifier_outputs, dim=1)
        return classifier_outputs


classifier = [
    nn.Sequential(
        nn.Linear(960, 1280),
        nn.Hardswish(),
        nn.Dropout(p=0.2, inplace=True),
        nn.Linear(1280, 10)  # Output for each digit (0-9)
    ) for _ in range(6)
]
model.classifier = Classifier()


csv_file_path = './datasets/avg_price_ocr/labels/digits_and_unit.csv'
transform = transforms.Compose([
    transforms.Resize((640, 640)),
    transforms.ToTensor(),
    transforms.Normalize((0.5,), (0.5,)),  # Normalize images
])
train_dataset = CustomDigitDataset(csv_file=csv_file_path, transform=transform)

# Create data loaders for training and testing
test_loader = DataLoader(train_dataset, batch_size=1, shuffle=False)
model.load_state_dict(torch.load("./avg_price_ocr_mobilenet_best.pt"))
model.eval()
model.to(device)
correct = 0
total = 0
all_correct = 0
all_total = 0
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        images = images.to(device)
        labels = labels.to(device)
        outputs = model(images)
        predicted = torch.argmax(outputs.data, dim=1)
        labels = torch.argmax(labels.data, dim=1)
        total += labels.size(0)
        correct_this_time = (predicted == labels).sum().item()
        correct += correct_this_time
        if correct_this_time == 6:
            all_correct += 1
        all_total += 1

print('Accuracy of the network digits: %d %%' % (
    100 * correct / total / 6))
print('Accuracy of the network on the test images: %d %%' % (
    100 * all_correct / all_total))
