import tkinter as tk
from PIL import Image, ImageTk
import os


def to_character_list(num_str):
    result = []
    int_float = num_str.split('.')
    int_part = int_float[0]
    if len(int_float) == 1:
        float_part = 0
    else:
        float_part = int(int_float[1])
    result += list(int_part)
    while len(result) < 3:
        result.insert(0, '0')
    result += list(str(float_part))
    while len(result) < 5:
        result.append('0')
    return result


unit_dict = {
    'kg': 'kg',
    'Kg': 'kg',
    'kG': 'kg',
    'KG': 'kg',
    'L': 'L',
    'l': 'L',
    'lit': 'L',
    'LIT': 'L',
    'st': 'st',
    'St': 'st',
    'sT': 'st',
    'ST': 'st',
}

unit_name_to_id = {
    'kg': '0',
    'L': '1',
    'st': '2',
}


class LabelNumber:
    def __init__(self, root, image_folder):
        self.root = root
        self.image_folder = image_folder
        self.image_files = self.get_image_files()
        self.current_image_index = 0

        self.image_label = tk.Label(root)
        self.image_label.pack()

        self.prev_button = tk.Button(
            root, text="Previous", command=self.show_previous_image)
        self.prev_button.pack()

        self.next_button = tk.Button(
            root, text="Next", command=self.show_next_image)
        self.next_button.pack()

        self.avg_price_input = tk.Entry(root)
        self.avg_price_input.pack()

        self.unit_input = tk.Entry(root)
        self.unit_input.pack()

        self.submit_button = tk.Button(
            root, text="Submit", command=self.on_submit)
        self.submit_button.pack()

        self.show_current_image()

    def on_submit(self):
        avg_price = self.avg_price_input.get()
        self.avg_price_input.delete(0, len(avg_price))
        unit = self.unit_input.get()
        avg_price_character_list = to_character_list(avg_price)
        avg_price_character_list_str = ','.join(avg_price_character_list)
        unit_id = unit_name_to_id[unit_dict[unit]]
        with open(f'./datasets/avg_price_ocr/labels/digits_and_unit.csv', 'a') as f:
            f.write(
                f'{self.image_files[self.current_image_index]},{avg_price_character_list_str},{unit_id}\n')
        self.show_next_image()

    def get_image_files(self):
        image_files = []
        for filename in os.listdir(self.image_folder):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.bmp')):
                image_files.append(os.path.join(self.image_folder, filename))
        return image_files

    def show_current_image(self):
        if self.image_files:
            image_path = self.image_files[self.current_image_index]
            image = Image.open(image_path)
            image = image.resize((224, 112))
            photo = ImageTk.PhotoImage(image)
            self.image_label.config(image=photo)
            self.image_label.image = photo

    def show_previous_image(self):
        if self.current_image_index > 0:
            self.current_image_index -= 1
            self.show_current_image()

    def show_next_image(self):
        if self.current_image_index < len(self.image_files) - 1:
            self.current_image_index += 1
            self.show_current_image()


if __name__ == "__main__":
    root = tk.Tk()
    image_folder = "./datasets/avg_price_ocr/images"
    app = LabelNumber(root, image_folder)
    root.mainloop()
