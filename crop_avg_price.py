import os
from ultralytics import YOLO
from PIL import Image

if __name__ == '__main__':
    model = YOLO('./direct_avg_price.pt')
    files = os.listdir('./datasets/direct_avg_price/images')
    fid = 0
    with open("./datasets/avg_price_ocr/labels/types.csv", "w") as f:
        for file in files:
            try:
                results = model.predict(
                    f'./datasets/direct_avg_price/images/{file}', save=True, imgsz=640, conf=0.7)
            except:
                continue
            for result in results:
                image_global = Image.open(
                    f'./datasets/direct_avg_price/images/{file}')
                for xyxy in result.boxes.xyxy:
                    image = image_global.copy()
                    image = image.crop(tuple(xyxy.tolist()))
                    image.save(f'./datasets/avg_price_ocr/images/{fid}.png')
                    f.write(
                        f'{fid},{int(result.boxes.cls[0])}\n')
                    fid += 1
