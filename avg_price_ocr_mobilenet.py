import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import transforms, datasets
from torchvision.models import mobilenet_v3_large, MobileNet_V3_Large_Weights
from torch.utils.data import Dataset, DataLoader
import pandas as pd
from PIL import Image

device = torch.device("cpu")


class CustomDigitDataset(Dataset):
    def __init__(self, csv_file, transform=None):
        self.data = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        img_path = self.data.iloc[idx, 0]
        digits = self.data.iloc[idx, 2:8].values.astype('int')
        # todo: the last digit has only 3 classes
        data = [torch.nn.functional.one_hot(
            torch.tensor(x), num_classes=10) for x in digits]
        digits = torch.stack(
            data
        )
        digits = digits.float()
        # Load image
        image = Image.open(img_path)
        image = image.convert('RGB')
        image = image.resize((640, 320))
        if self.transform:
            image = self.transform(image)

        return image, digits


model = mobilenet_v3_large(weights=MobileNet_V3_Large_Weights.DEFAULT)


class Classifier(nn.Module):
    def __init__(self):
        super(Classifier, self).__init__()
        classifier = [
            nn.Sequential(
                nn.Linear(960, 1280),
                nn.Hardswish(),
                nn.Dropout(p=0.2, inplace=True),
                nn.Linear(1280, 10)  # Output for each digit (0-9)
            ) for _ in range(6)
        ]
        self.classifier = nn.ModuleList(classifier)

    def forward(self, x):
        batch_size = x.size(0)
        classifier_outputs = []
        for i in range(6):
            classifier_output = self.classifier[i](x)
            classifier_outputs.append(classifier_output)
        classifier_outputs = torch.stack(classifier_outputs, dim=1)
        return classifier_outputs


model.classifier = Classifier()

csv_file_path = './datasets/avg_price_ocr/labels/digits_and_unit.csv'
transform = transforms.Compose([
    transforms.Resize((640, 320)),
    transforms.ToTensor(),
    transforms.Normalize((0.5,), (0.5,)),  # Normalize images
])
train_dataset = CustomDigitDataset(csv_file=csv_file_path, transform=transform)
train_loader = DataLoader(train_dataset, batch_size=16, shuffle=True)
optimizer = optim.SGD(model.parameters(), lr=0.00001, momentum=0.9)
criterion = nn.CrossEntropyLoss()

model.load_state_dict(torch.load("./avg_price_ocr_mobilenet_best.pt"))
# Training loop
n_epochs = 50
model.to(device)
for epoch in range(n_epochs):
    print(f'Epoch {epoch+1}\n-------------------------------')
    model.train()
    print(f'{len(train_loader)} batches')
    for batch_idx, (data, targets) in enumerate(train_loader):
        print(f'batch {batch_idx} forwarding...')
        data = data.to(device)
        targets = targets.to(device)
        optimizer.zero_grad()
        outputs = model(data)
        loss = sum([F.mse_loss(output, target)
                    for output, target in zip(outputs.unbind(1), targets.unbind(1))])
        print(f'batch {batch_idx} backwarding with loss={loss} ...')
        loss.backward()
        optimizer.step()
    torch.save(model.state_dict(), f"./ocr_runs/train/{epoch}.pt")
